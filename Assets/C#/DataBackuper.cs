﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using UnityEngine.UI;
using System.IO;

public static class DataBackuper
{
    public static void CheckPlayerData(MenuInfo playerData)
    {
        string directory = Path.Combine(Application.persistentDataPath, "11hka2oHf8agH2"); // Путь к бэкапу класс-файла
        if(File.Exists(Path.Combine(directory, "pl.txt"))) // Файл уже существует
            DataBackuper.ReadFromDataFile(playerData);
        else // Нужно создать
        {
            Directory.CreateDirectory(directory);
            DataBackuper.WriteToDataFile();
        }
    }

    public static void ReadFromDataFile(MenuInfo playerData)
    {
        string directory = Path.Combine(Application.persistentDataPath, "11hka2oHf8agH2"); // Путь к бэкапу класс-файла
        string rowData = File.ReadAllText(Path.Combine(directory, "pl.txt"));
        string afterDecrypt = EncryptionUtil.Decrypt(rowData, "ygidfn83hr89fn9hu23fHIUJBi3jor8e");
        JsonUtility.FromJsonOverwrite(afterDecrypt, playerData);
    }

    public static void WriteToDataFile()
    {
        string directory = Path.Combine(Application.persistentDataPath, "11hka2oHf8agH2"); // Путь к бэкапу класс-файла
        MenuInfo data = Resources.Load("Player Data") as MenuInfo;
        Debug.Log(JsonUtility.ToJson(data));
        string afterEncrypt = EncryptionUtil.Decrypt(JsonUtility.ToJson(data), "ygidfn83hr89fn9hu23fHIUJBi3jor8e");
        File.WriteAllText(Path.Combine(directory, "pl.txt"), afterEncrypt);
    }
}

// Прячем данные свои ( -_-)
	public class EncryptionUtil {
		private static string KEY;// = "oi9cf12e09gADAJDk2qnGhsnwmrKsD1l"; // pick some other 32 chars
		private static byte[] KEY_BYTES;// = Encoding.UTF8.GetBytes(KEY);

		public static string Encrypt(string plainText, string k) {
			KEY = k;
			KEY_BYTES = Encoding.UTF8.GetBytes(KEY);
			// Check arguments.
			if (plainText == null || plainText.Length <= 0)
				throw new System.ArgumentNullException("plainText");

			byte[] encrypted;
			// Create an AesManaged object
			// with the specified key and IV.
			using (Rijndael algorithm = Rijndael.Create()) {
				algorithm.Key = KEY_BYTES;
				algorithm.Mode = CipherMode.CBC;

				// Create a decrytor to perform the stream transform.
				var encryptor = algorithm.CreateEncryptor(algorithm.Key, algorithm.IV);

				// Create the streams used for encryption.
				using (var msEncrypt = new MemoryStream()) {
					using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write)) {
						using (var swEncrypt = new StreamWriter(csEncrypt)) {
							// Write IV first
							msEncrypt.Write(algorithm.IV, 0, algorithm.IV.Length);
							//Write all data to the stream.
							swEncrypt.Write(plainText);
						}
						encrypted = msEncrypt.ToArray();
					}
				}
			}

			// Return the encrypted bytes from the memory stream.
			return System.Convert.ToBase64String(encrypted);
		}

		public static string Decrypt(string cipherText, string k) {
			KEY = k;
			KEY_BYTES = Encoding.UTF8.GetBytes(KEY);
			// Check arguments.
			if (cipherText == null || cipherText.Length <= 0)
				throw new System.ArgumentNullException("cipherText");

			// Declare the string used to hold
			// the decrypted text.
			string plaintext = null;

			// Create an AesManaged object
			// with the specified key and IV.
			using (Rijndael algorithm = Rijndael.Create()) {
				algorithm.Key = KEY_BYTES;
				algorithm.Mode = CipherMode.CBC;

				// Get bytes from input string
				byte[] cipherBytes = System.Convert.FromBase64String(cipherText);

				// Create the streams used for decryption.
				using (MemoryStream msDecrypt = new MemoryStream(cipherBytes)) {
					// Read IV first
					byte[] IV = new byte[16];
					msDecrypt.Read(IV, 0, IV.Length);

					// Assign IV to an algorithm
					algorithm.IV = IV;

					// Create a decrytor to perform the stream transform.
					var decryptor = algorithm.CreateDecryptor(algorithm.Key, algorithm.IV);

					using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read)) {
						using (var srDecrypt = new StreamReader(csDecrypt)) {
							// Read the decrypted bytes from the decrypting stream
							// and place them in a string.
							plaintext = srDecrypt.ReadToEnd();
						}
					}
				}
			}
			return plaintext;
		}
	}
