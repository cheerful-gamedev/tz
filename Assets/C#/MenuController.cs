﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

using FTRuntime;
using FTRuntime.Yields;

public class MenuController : MonoBehaviour
{
    [SerializeField] private MenuInfo playerData, shopData;
    [SerializeField] private Transform mainContent; // Объект в Canvas, хранящий в себе главной контент сцены
    [SerializeField] private GameObject zombosPlane; // Ссылка на префаб карточки с Зомбо
    [SerializeField] private Sprite[] moneyTypeSprites; // Плашки фона для разный валюты
    [SerializeField] private List<GameObject> contents = new List<GameObject>(); // Ссылка на все контекты меню
    [SerializeField] private Text[] downPanel; // Нижние текста параметров (урон, щит, скорость)
    [SerializeField] private Text[] cash; // Деньги и алмазы, которыми обладает игрок
    [SerializeField] private List<UpgradesPlane> upgradesPlanes = new List<UpgradesPlane>(); // Строки в Upgrades Content
    [SerializeField] private GameObject mainMusic; // Главная тема, играющая на фоне
    [SerializeField] private GameObject moneyPlanePFB; // Ссылка на префаб карточки с реал. покупкой
    [SerializeField] private List<MoneyPlane> moneysPlane; // Все возможные покупки за реал

    private List<Transform> zombosPlanes = new List<Transform>(); // Уже существующие карточки на сцене
    private List<int> indexesOfzombosPlanes = new List<int>(); // Индексы Зомбо уже отрисованных карт
    private List<Transform> moneyPlanes = new List<Transform>(); // Уже существующие карточки на сцене

    AudioSource audio;

    // TODO: сделать класс для карточек Зомбо, хранящие в себе ID Зомбо и прочую необходимую инфу
    void Start()
    {
        DataBackuper.CheckPlayerData(playerData);
        playerData.GetStartKit(); // Обновляем данные в класс-файле

        audio = GetComponent<AudioSource>();

        if(playerData.GetZombo(playerData.GetCurSelectedZombo()) == null)
            playerData.SetCurSelectedZombo(0);

        PlanetsContent();

        if(GameObject.Find("Music") == null)
            Instantiate(mainMusic);
    }

    // Отрисовываем Зомбо контент
    public void ZombiesContent()
    {
        SetContentActive("Zombies Content");

        // Удаляем уже отрисованные карточки
        foreach (Transform tr in zombosPlanes)
            Destroy(tr.gameObject);

        zombosPlanes.Clear();
        indexesOfzombosPlanes.Clear();

        Transform content = mainContent.Find("Zombies Content/Scroll View/Viewport/Content");
        foreach(ZomboInfo zombo in shopData.GetAllZombo())
        {
            Transform plane = Instantiate(zombosPlane, content).transform;
            zombosPlanes.Add(plane);

            // Меняем картинку Зомбо
            plane.Find("Zombie Image").GetComponent<Image>().sprite = zombo.Sprite;

            int num = zombosPlanes.Count - 1; // Нужна отдельная переменная, иначе в delegate он передает ссылку (значение постоянно равно последнему индексу)
            plane.GetComponent<Button>().onClick.AddListener(delegate { this.SelectZombie(zombo.GetID(), num); });

            Transform moneyType = plane.Find("MoneyType_fon");
            if (playerData.GetZombo(zombo.GetID()) == null) // Если Зомбо еще не куплен
            {
                plane.GetComponentInChildren<Text>().text = zombo.GetPrice().ToString();
                moneyType.GetComponent<Button>().onClick.AddListener(delegate { this.BuyZombie(zombo.GetID(), zombo.GetPriceType()); });

                if (zombo.GetPriceType() == PriceType.Diamond) // Меняем фон ценника в зависимости от валюты
                    moneyType.GetComponent<Image>().sprite = moneyTypeSprites[1];
            }
            else // Если Зомбо уже куплен
            {
                if(indexesOfzombosPlanes.IndexOf(zombo.GetID()) != -1) // Если карточка купленного Зомбо с таким ID уже отрисована
                {
                    zombosPlanes.Remove(plane);
                    Destroy(plane.gameObject);
                    continue;
                }
                else // Если карточка с купленным Зомбо отрисовывается впервые
                    Destroy(moneyType.gameObject);
            }

            indexesOfzombosPlanes.Add(zombo.GetID());

            if (playerData.GetCurSelectedZombo() == zombo.GetID())
                plane.Find("Zombie Selected").GetComponent<Image>().enabled = true;
        }

        // Выделяем одного из Зомбо
        SelectZombie(playerData.GetCurSelectedZombo(), indexesOfzombosPlanes.IndexOf(playerData.GetCurSelectedZombo()));

        // Спавн неизвестного Зомбо на будущее
        zombosPlanes.Add(Instantiate(zombosPlane, content).transform);

        UpdateDownPanel();
        UpdateCash();
    }

    // Выбираем Зомбо
    public void SelectZombie(int id, int num)
    {
        // Снимаем выделение со всех Зомбо
        try
        {
            foreach (Transform tr in zombosPlanes)
                tr.Find("Zombie Selected").GetComponent<Image>().enabled = false;
        }
        catch { Debug.LogError("Ошибка: не найден компонент Image в карточке Зомбо"); }

        // Выделяем Зомбо, на который кликнули, но только если он у нас куплен
        if (playerData.GetZombo(id) != null)
        {
            zombosPlanes[num].Find("Zombie Selected").GetComponent<Image>().enabled = true;
            playerData.SetCurSelectedZombo(id);
            UpdateDownPanel();
        }
        else if (playerData.GetZombo(playerData.GetCurSelectedZombo()) != null) // Возвращаем выделение прошлому Зомбо, если таковой куплен
        {
            zombosPlanes[playerData.GetCurSelectedZombo()].Find("Zombie Selected").GetComponent<Image>().enabled = true;
        }
        else // Или задаем выделение на первого Зомбо
        {
            zombosPlanes[0].Find("Zombie Selected").GetComponent<Image>().enabled = true;
            playerData.SetCurSelectedZombo(0);
        }
    }

    // Обновление параметров нижней панели
    public void UpdateDownPanel()
    {
        ZomboInfo selectedZombo = playerData.GetZombo(playerData.GetCurSelectedZombo());
        downPanel[0].text = selectedZombo.GetDamage().ToString();
        downPanel[1].text = selectedZombo.GetArmor().ToString();
        downPanel[2].text = selectedZombo.GetSpeed().ToString();

        DataBackuper.WriteToDataFile();
    }

    // Обновление средств в верхней части экрана
    public void UpdateCash()
    {
        cash[0].text = playerData.GetMoney().ToString();
        cash[1].text = playerData.GetDiamonds().ToString();
    }

    // Покупаем Зомбо
    public void BuyZombie(int id, PriceType priceType)
    {
        ZomboInfo zombo = shopData.GetZombo(id, priceType);
        if (zombo == null || playerData.GetAllZombo().Find(z => z.GetID() == zombo.GetID()) != null)
        {
            Debug.Log("Такой Зомбо не найден в класс-файле магазина или уже куплен");
            return;
        }

        switch(zombo.GetPriceType())
        {
            case PriceType.Money:
                if (playerData.GetMoney() >= zombo.GetPrice())
                {
                    playerData.SetMoney(playerData.GetMoney() - zombo.GetPrice()); // Списываем валюту
                    playerData.BuyNewZombo(id); // Добавляем в класс-файл нового Зомбо
                    playerData.SetCurSelectedZombo(id);
                    audio.Play();
                    ZombiesContent();
                }
                else
                    Debug.Log("Не достаточно денег");
            break;

            case PriceType.Diamond:
                if (playerData.GetDiamonds() >= zombo.GetPrice())
                {
                    playerData.SetDiamonds(playerData.GetDiamonds() - zombo.GetPrice()); // Списываем валюту
                    playerData.BuyNewZombo(id); // Добавляем в класс-файл нового Зомбо
                    playerData.SetCurSelectedZombo(id);
                    audio.Play();
                    ZombiesContent();
                }
                else
                    Debug.Log("Не достаточно алмазов");
                break;

            case PriceType.Video:
                if (playerData.GetVideos() >= zombo.GetPrice())
                {
                    playerData.SetVideos(playerData.GetVideos() - zombo.GetPrice()); // Списываем валюту
                    playerData.BuyNewZombo(id); // Добавляем в класс-файл нового Зомбо
                    playerData.SetCurSelectedZombo(id);
                    audio.Play();
                    ZombiesContent();
                }
                else
                    Debug.Log("Не достаточно просмотренных видео");
                break;
        }
    }

    // Отрисовываем контент с Апгрейдами
    public void UpgradesContent()
    {
        SetContentActive("Upgrades Content");

        ZomboInfo selectedZombo_shop = shopData.GetZombo(playerData.GetCurSelectedZombo());
        ZomboInfo selectedZombo = playerData.GetZombo(playerData.GetCurSelectedZombo());

        string[] parametersName = new string[] {"Damage", "Armor", "Speed", "TapMultiplier"};

        // Апгрейды, касающиеся конкретного Зомбо
        for (int i = 0; i < parametersName.Length; i++)
        {
            // Поиск купленного параметра
            Parameter curParameter = selectedZombo.GetParameterByName(parametersName[i]);
            int parameterID = -1;
            if (curParameter != null) // Если такого параметра у Зомби есть
                parameterID = curParameter.GetID();

            Button button = upgradesPlanes[i].Button;
            // Поиск апгрейда для параметра
            Parameter nextParameter = selectedZombo_shop.GetParameterByName(parametersName[i], parameterID + 1);
            if (nextParameter != null) // Если существует апгрейд для параметра
            {
                // Меняем подложку взависимости от валюты
                switch(nextParameter.GetPriceType())
                {
                    case PriceType.Money: button.GetComponent<Image>().sprite = moneyTypeSprites[0]; break;
                    case PriceType.Diamond: button.GetComponent<Image>().sprite = moneyTypeSprites[1]; break;
                    case PriceType.Video: button.GetComponent<Image>().sprite = moneyTypeSprites[2]; break;
                }

                button.onClick.RemoveAllListeners();
                button.onClick.AddListener(delegate { this.BuyUpgrades(nextParameter, selectedZombo, button.transform.Find("Salute").GetComponent<SwfClipController>()); });
                upgradesPlanes[i].Cost.text = nextParameter.GetPrice().ToString();
                if(i < 3)
                    upgradesPlanes[i].Up.text = ((float)selectedZombo.GetParameterValueByName("Start"+parametersName[i]) + nextParameter.GetValue()).ToString();//(nextParameter.GetValue() - curParameter.GetValue());
                else
                    upgradesPlanes[i].Up.text = "x"+(nextParameter.GetValue()+1);
            }
            else // Если параметр вкачен на максимально возможный уровень
            {
                button.onClick.RemoveAllListeners();
                upgradesPlanes[i].Cost.text = "Maximum";
                if(i < 3)
                    upgradesPlanes[i].Up.text = selectedZombo.GetParameterValueByName(parametersName[i]).ToString();
                else
                    upgradesPlanes[i].Up.text = "x"+(curParameter.GetValue()+1);
            }
        }

        string[] cashUpgradesName = new string[] {"Money", "Diamonds"};

        for(int i=0; i<cashUpgradesName.Length; i++)
        {
            // Поиск купленного параметра
            Parameter curParameter = playerData.GetCashUpgradeByName(cashUpgradesName[i]);
            int parameterID = -1;
            if (curParameter != null) // Если такого параметра у Зомби есть
                parameterID = curParameter.GetID();

            Button button = upgradesPlanes[i+4].Button;
            // Поиск апгрейда для параметра
            Parameter nextParameter = shopData.GetCashUpgradeByName(cashUpgradesName[i], parameterID + 1);
            if (nextParameter != null) // Если существует апгрейд для параметра
            {
                // Меняем подложку взависимости от валюты
                switch(nextParameter.GetPriceType())
                {
                    case PriceType.Money: button.GetComponent<Image>().sprite = moneyTypeSprites[0]; break;
                    case PriceType.Diamond: button.GetComponent<Image>().sprite = moneyTypeSprites[1]; break;
                    case PriceType.Video: button.GetComponent<Image>().sprite = moneyTypeSprites[2]; break;
                }

                button.onClick.AddListener(delegate { this.BuyCashUpgrades(nextParameter, button.transform.Find("Salute").GetComponent<SwfClipController>()); });
                upgradesPlanes[i+4].Cost.text = nextParameter.GetPrice().ToString();
                upgradesPlanes[i+4].Up.text = "x"+nextParameter.GetValue();
            }
            else // Если параметр вкачен на максимально возможный уровень
            {
                button.onClick.RemoveAllListeners();
                upgradesPlanes[i+4].Cost.text = "Maximum";
                upgradesPlanes[i+4].Up.text = "x"+curParameter.GetValue();
            }
        }

        UpdateDownPanel();
        UpdateCash();
    }

    // Покупаем апгрейд для Зомбо
    public void BuyUpgrades(Parameter param, ZomboInfo zombo, SwfClipController salute)
    {
        if(zombo.GetParameters().Find(p => p.GetName().Equals(param.GetName()) && p.GetID() >= param.GetID()) != null)
            return;

        switch (param.GetPriceType())
        {
            case PriceType.Money:
                if (playerData.GetMoney() >= param.GetPrice())
                {
                    int money = playerData.GetMoney() - param.GetPrice();
                    playerData.SetMoney(money); // Списываем валюту
                    zombo.BuyParameter(param);
                    zombo.RefreshAllCharacteristics();
                    salute.Play(true);
                    audio.Play();
                    UpgradesContent();
                }
                else
                    Debug.Log("Не достаточно денег");
                break;

            case PriceType.Diamond:
                if (playerData.GetDiamonds() >= param.GetPrice())
                {
                    int diamonds = playerData.GetDiamonds() - param.GetPrice();
                    playerData.SetDiamonds(diamonds); // Списываем валюту
                    zombo.BuyParameter(param);
                    zombo.RefreshAllCharacteristics();
                    salute.Play(true);
                    audio.Play();
                    UpgradesContent();
                }
                else
                    Debug.Log("Не достаточно алмазов");
                break;

            case PriceType.Video:
                if (playerData.GetVideos() >= param.GetPrice())
                {
                    int videos = playerData.GetVideos() - param.GetPrice();
                    playerData.SetVideos(videos); // Списываем валюту
                    zombo.BuyParameter(param);
                    zombo.RefreshAllCharacteristics();
                    salute.Play(true);
                    audio.Play();
                    UpgradesContent();
                }
                else
                    Debug.Log("Не достаточно просмотренных видео");
                break;
        }
    }

    // Покупаем апгрейд получаемой валюты
    public void BuyCashUpgrades(Parameter param, SwfClipController salute)
    {
        if(playerData.GetCashUpgradeByName(param.GetName()).GetID() >= param.GetID())
            return;

        switch (param.GetPriceType())
        {
            case PriceType.Money:
                if (playerData.GetMoney() >= param.GetPrice())
                {
                    int money = playerData.GetMoney() - param.GetPrice();
                    playerData.SetMoney(money); // Списываем валюту
                    playerData.BuyCashUpgrade(param);
                    salute.Play(true);
                    audio.Play();
                    UpgradesContent();
                }
                else
                    Debug.Log("Не достаточно денег");
                break;

            case PriceType.Diamond:
                if (playerData.GetDiamonds() >= param.GetPrice())
                {
                    int diamonds = playerData.GetDiamonds() - param.GetPrice();
                    playerData.SetDiamonds(diamonds); // Списываем валюту
                    playerData.BuyCashUpgrade(param);
                    salute.Play(true);
                    audio.Play();
                    UpgradesContent();
                }
                else
                    Debug.Log("Не достаточно алмазов");
                break;

            case PriceType.Video:
                if (playerData.GetVideos() >= param.GetPrice())
                {
                    int videos = playerData.GetVideos() - param.GetPrice();
                    playerData.SetVideos(videos); // Списываем валюту
                    playerData.BuyCashUpgrade(param);
                    salute.Play(true);
                    audio.Play();
                    UpgradesContent();
                }
                else
                    Debug.Log("Не достаточно просмотренных видео");
                break;
        }
    }

    // Отрисовываем список уровней
    public void PlanetsContent()
    {
        SetContentActive("Planets Content");

        UpdateDownPanel();
        UpdateCash();
    }

    // Отрисовываем список покупок за реал
    public void MoneyContent()
    {
        SetContentActive("Money Content");
        
        // Удаляем уже отрисованные карточки
        foreach (Transform tr in moneyPlanes)
            Destroy(tr.gameObject);

        moneyPlanes.Clear();

        Transform content = mainContent.Find("Money Content/Scroll View/Viewport/Content");
        foreach(MoneyPlane moneyPlane in moneysPlane)
        {
            Transform plane = Instantiate(moneyPlanePFB, content).transform;
            moneyPlanes.Add(plane);

            // Меняем картинку Зомбо
            plane.Find("Money Image").GetComponent<Image>().sprite = moneyPlane.Image;

            int num = moneyPlanes.Count - 1; // Нужна отдельная переменная, иначе в delegate он передает ссылку (значение постоянно равно последнему индексу)

            Transform moneyType = plane.Find("MoneyType_fon");
            moneyType.GetComponent<Button>().onClick.AddListener(delegate { this.BuyMoney(num); });
            // Меняем фон кнопки
            if(moneyPlane.MoneyType == PriceType.Money)
                moneyType.GetComponent<Image>().sprite = moneyTypeSprites[2];
            else if(moneyPlane.MoneyType == PriceType.Diamond)
                moneyType.GetComponent<Image>().sprite = moneyTypeSprites[3];

            moneyType.Find("Cost").GetComponent<Text>().text = moneyPlane.Price.ToString();
            moneyType.Find("Number").GetComponent<Text>().text = moneyPlane.Number.ToString();
        }

        UpdateDownPanel();
        UpdateCash();
    }

    // Покупка игровою валюты за реал
    public void BuyMoney(int id)
    {
        MoneyPlane moneyPlane = moneysPlane[id];
        switch (moneyPlane.MoneyType)
        {
            case PriceType.Money:
            Debug.Log("Money " + moneyPlane.Number);
                // if (/* Тут проверка на оплату */)
                // {
                //     int money = playerData.GetMoney() + moneyPlane.Number;
                //     playerData.SetMoney(money); // Обновляем валюту
                //     audio.Play();
                //     UpgradesContent();
                // }
                // else
                //     Debug.Log("Оплата не прошла");
                break;

            case PriceType.Diamond:
            Debug.Log("Diamond " + moneyPlane.Number);
                // if (playerData.GetDiamonds() >= moneyPlane.Number)
                // {
                //     int diamonds = playerData.GetDiamonds() + moneyPlane.Number;
                //     playerData.SetDiamonds(diamonds); // Обновляем валюту
                //     audio.Play();
                //     UpgradesContent();
                // }
                // else
                //     Debug.Log("Оплата не прошла");
                break;

            case PriceType.Video:
            Debug.Log("Video " + moneyPlane.Number);
                // if (playerData.GetVideos() >= moneyPlane.Number)
                // {
                //     int videos = playerData.GetVideos() + moneyPlane.Number;
                //     playerData.SetVideos(videos); // Обновляем валюту
                //     audio.Play();
                //     UpgradesContent();
                // }
                // else
                //     Debug.Log("Оплата не прошла");
                break;
        }
    }

    // Отображение контента по имени
    public void SetContentActive(string contentName)
    {
        // Выключение всех контентов
        foreach (GameObject cont in contents)
            cont.SetActive(false);

        // Включение текущего контента
        contents.Find(c => c.name.Equals(contentName)).SetActive(true);
    }

    public void Fight(int id)
    {
        SceneManager.LoadScene(id);
    }

    [System.Serializable]
    public class UpgradesPlane
    {
        public Button Button;
        public Text Cost, Up; // Текст с ценой и текст отображающий прирост
    }

    [System.Serializable]
    public class MoneyPlane
    {
        public Sprite Image;
        public float Price; // Цена
        public int Number; // Кол-во получаемых деняг
        public PriceType MoneyType; // Вид получаемых денег
    }
}
