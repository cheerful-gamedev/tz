﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FTRuntime;
using FTRuntime.Yields;

public class Scenka : MonoBehaviour
{
    [SerializeField] private float rayDistance = 1.5f;
    [SerializeField] private SwfClipController clipController;
    [SerializeField] private SwfClip clip;
    [SerializeField] private Transform rayPoint;
    [SerializeField] private bool angry;

    private LayerMask layer;

    void Awake()
    {
        layer = LayerMask.GetMask("Zomb");
    }

    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(rayPoint.position, rayPoint.right * transform.localScale.x, rayDistance, layer);
        Debug.DrawRay(rayPoint.position, rayPoint.right * transform.localScale.x, Color.red);
        if (hit && hit.transform != transform)
        {
            Debug.Log(gameObject.name+" kick "+hit.transform.name);
            if (!clip.sequence.Equals("damage"))
            {
                clipController.loopMode = SwfClipController.LoopModes.Once;
                clipController.Play("damage");
                hit.transform.SendMessage("TakeDamage");
            }
        }
        else if (!clip.sequence.Equals("damage") || (clip.sequence.Equals("damage") && clip.currentFrame == clip.frameCount-1))
        {
            if (!clip.sequence.Equals("run"))
            {
                clipController.loopMode = SwfClipController.LoopModes.Loop;
                clipController.Play("run");
            }
            transform.Translate(transform.right * transform.localScale.x * Time.deltaTime * 10);


            if (transform.position.x >= 10)
                transform.localScale = new Vector3(-1, 1, 1);
            else if (transform.position.x <= -10)
                transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void TakeDamage()
    {
        transform.localScale = new Vector3(-transform.localScale.x, 1, 1);
        clipController.Play("stop");
    }
}
