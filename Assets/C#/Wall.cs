﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FTRuntime;
using FTRuntime.Yields;

public class Wall : MonoBehaviour
{
    [SerializeField] private SwfClipController clipController;
    private Obj myClass; // Информация о Башне
    private int destructionIndex = 1; // Индекс степени разрушенности
    private bool destroyed; // Стена уже уничтожена
    private float takeDamageTimer; // Время с последнего удара

    GameManager GM;

    void Awake()
    {
        GM = GameObject.Find("Game Manager").GetComponent<GameManager>();
    }

    public void Initialization(Obj obj) { myClass = obj; }

    void Update() { takeDamageTimer += Time.deltaTime; }

    public void TakeDamage(float damage)
    { 
        if(destroyed) return;

        takeDamageTimer = 0;
        if(clipController.isStopped)
            clipController.Play("damage_0"+destructionIndex+"_0"+Random.Range(1,3));
        myClass.TakeDamage(damage);
    }

    // public void Die()
    // {
    //     destructionIndex = destructionIndex+1 >=3 ? 3 : destructionIndex+1;
    //     if (destructionIndex >= 3)
    //     {   
    //         GM.DeleteWall(myClass);
    //         Destroy(GetComponent<BoxCollider>());
    //     }
    //     else
    //         myClass.SetCurHealth(myClass.GetHealth());
    //     clipController.Play(destructionIndex.ToString());
    // }

    public void Die()
    { 
        if(destructionIndex == 1)
        {
            // clipController.playMode = SwfClipController.PlayModes.Forward;
            clipController.Play("destruction_"+destructionIndex);
            destructionIndex = 2;
            myClass.SetCurHealth(myClass.GetHealth());
        }
        else if(destructionIndex == 2)
        {
            // clipController.playMode = SwfClipController.PlayModes.Forward;
            clipController.Play("destruction_"+destructionIndex);
            destructionIndex = 3;
            myClass.SetCurHealth(myClass.GetHealth());
        }
        else if(destructionIndex == 3)
        {
            destroyed = true;
            clipController.transform.parent = transform.parent;
            // clipController.playMode = SwfClipController.PlayModes.Forward;
            clipController.Play("destruction_"+destructionIndex);
            clipController.transform.GetComponent<LayerController>().enabled = true;
            // LayerController lc = clipController.gameObject.AddComponent<LayerController>();
            // lc.offset = 10;
            GM.DeleteWall(myClass);
            Destroy(gameObject);
        }
    }
}
