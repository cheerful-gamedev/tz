﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Obj
{
    private GameObject Object;
    private float Damage;
    private float Health, CurHealth;
    private float Speed;
    private float Armor;
    private float Regeneration;

    // Только для Башен (точки вокруг башни, где будут стоять Зомбо)
    private List<TowerPosition> TowerPositions;

    public Obj(GameObject Object, float Damage, float Health, float Speed, float Armor, float Regeneration, bool isItTower = false)
    {
        this.Object = Object;
        this.Damage = Damage;
        this.Health = Health;
        this.Speed = Speed;
        this.Armor = Armor;
        this.Regeneration = Regeneration;
        this.CurHealth = Health;

        if(Object != null)
            Object.SendMessage("Initialization", this);

        if(isItTower)
        {
            TowerPositions = new List<TowerPosition>();
            Vector3 towerPos = Object.transform.position;
            towerPos.z -= 3.5f;

            Vector3 lastPos = towerPos;
            for(int i=0; i<12; i++)
            {
                var x = Mathf.Cos (i * 360) * 2.5f;
                var z = Mathf.Sin (i * 360) * 2.5f;
                TowerPositions.Add(new TowerPosition(towerPos + new Vector3(x, 0 ,z)));
            }
        }
    }

    public Obj(GameObject Object)
    {
        this.Object = Object;
        this.CurHealth = this.Health;

        Object.SendMessage("Initialization", this);
    }

    public GameObject GetGameObject() { return this.Object; }
    public void SetGameObject(GameObject value) { this.Object = value; value.SendMessage("Initialization", this); }

    public Vector3 GetRandomPosition()
    {
        int index = UnityEngine.Random.Range(0,TowerPositions.Count);
        // this.TowerPositions[index].Busy = true;
        return this.TowerPositions[index].Position;
    }

    public List<TowerPosition> GetTowerPositions() { return this.TowerPositions; }

    public float GetDamage() { return this.Damage; }
    public float GetHealth() { return this.Health; }
    public float GetCurHealth() { return this.CurHealth; }
    public float GetSpeed() { return this.Speed; }
    public float GetArmor() { return this.Armor; }
    public float GetRegeneration() { return this.Regeneration; }

    public void SetDamage(float value) { this.Damage = value; }
    public void SetHealth(float value) { this.Health = value; }
    public void SetCurHealth(float value) { this.CurHealth = value; }
    public void SetSpeed(float value) { this.Speed = value; }
    public void SetArmor(float value) { this.Armor = value; }
    public void SetRegeneration(float value) { this.Regeneration = value; }

    public void TakeDamage(float damage)
    {
        this.CurHealth = this.CurHealth - damage * (1 - this.Armor);
        if(this.CurHealth <= 0)
        {
            // this.CurHealth = 0;
            this.Object.SendMessage("Die");
        }
    }

    public void PlusRegeneration()
    {
        if(this.CurHealth + this.Regeneration <= this.Health)
            this.CurHealth += this.Regeneration;
        else
            this.Object.SendMessage("Regeneration");
    }
}

[Serializable]
public class TowerPosition
{
    public bool Busy;
    public Vector3 Position;

    public TowerPosition (Vector3 pos) { this.Position = pos; }
}

// public class Tower : Obj
// {
//     private float Regeneration;

//     public float GetRegeneration() { return this.Regeneration; }
//     public void SetRegeneration(float value) { this.Regeneration = value; }
// }