﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Taper : MonoBehaviour
{
    GameManager GM;
    [SerializeField] private GameObject zombosPlane; // Карточка Зомбо
    [SerializeField] private Transform content; // Контент, в котором будут отрисовываться карточки Зомбо
    private List<Transform> zombosPlanes = new List<Transform>(); // Уже существующие карточки на сцене
    private float tapReload = .3f, curTapReload; // Параметры для регулирования кол-во заспавненных в секунду

    [Header("Demo")]
    [SerializeField] private bool demoScene;

    public Text zombCount, zombCount2;
    
    private int zomboIndex;

    void Awake()
    {
        GM = /*GameObject.Find("Game Manager").*/GetComponent<GameManager>();
    }

    void Start()
    {
        DrawBoughtZombos();

        if(!demoScene) return;
        StartCoroutine(DemoScene());
    }

    IEnumerator DemoScene()
    {
        for(int i=0; i<1000; i++)
        {
            GM.AddNewZomb(0);
            zombCount.text = (i+1).ToString();
            zombCount2.text = (i+1).ToString();
            if(i == 999)
            {
                zombCount.text = "Спасибо\nза\nподписку!";
                zombCount2.text = "Спасибо\nза\nподписку!";
            }
            yield return new WaitForSeconds(.01f);
        }
    }

     void Update()
    {
        curTapReload += Time.deltaTime;

        #if UNITY_EDITOR
        if(Input.GetKeyDown(KeyCode.Alpha1) && GM.playerData.GetAllZombo().Find(z => z.GetID() == 0) != null)
            zomboIndex = 0;
        else if(Input.GetKeyDown(KeyCode.Alpha2) && GM.playerData.GetAllZombo().Find(z => z.GetID() == 1) != null)
            zomboIndex = 1;

        if (curTapReload >= tapReload && (Input.GetMouseButton(0) || Input.anyKey))
        {
            curTapReload = 0;
            for(int i=0; i< GM.playerData.GetZombo(zomboIndex).GetTapMultiplier(); i++)
                GM.AddNewZomb(zomboIndex);
        }
        #endif

        if(curTapReload >= tapReload && Input.touchCount > 0)
        {
            curTapReload = 0;
            // for(int i=0; i<Input.touches.Length && i<4; i++)
                // if(Input.GetTouch(i).phase == TouchPhase.Ended)
                    for (int j = 0; j < GM.playerData.GetZombo(zomboIndex).GetTapMultiplier(); j++)
                            GM.AddNewZomb(zomboIndex);
        }
    }

    public void SpawnZomb()
    {
        GM.AddNewZomb(zomboIndex);
    }

    void DrawBoughtZombos()
    {
        // Удаляем уже отрисованные карточки
        foreach (Transform tr in zombosPlanes)
            Destroy(tr.gameObject);

        zombosPlanes.Clear();

        foreach(ZomboInfo zombo in GM.playerData.GetAllZombo())
        {
            Transform plane = Instantiate(zombosPlane, content).transform;
            zombosPlanes.Add(plane);

            // Меняем картинку Зомбо
            plane.Find("Zombie Image").GetComponent<Image>().sprite = zombo.Sprite;

            plane.GetComponent<Button>().onClick.AddListener(delegate { this.ChangeZomboIndex(zombo.GetID()); });

            Destroy(plane.Find("MoneyType_fon").gameObject);
        }

        // Вырубаем выделение на всех Зомбо
            foreach(Transform pl in zombosPlanes)
                pl.Find("Zombie Selected").GetComponent<Image>().enabled = false;

        // Назначаем и выделяем нового Зомбо
        zombosPlanes[zomboIndex].Find("Zombie Selected").GetComponent<Image>().enabled = true;
    }

    // Меняем текущего Зомбо, если у нас такой куплен
    public void ChangeZomboIndex(int id)
    {
        if(GM.playerData.GetAllZombo().Find(z => z.GetID() == id) != null)
        {
            // Вырубаем выделение на всех Зомбо
            foreach(Transform pl in zombosPlanes)
                pl.Find("Zombie Selected").GetComponent<Image>().enabled = false;

            // Назначаем и выделяем нового Зомбо
            zomboIndex = id;
            zombosPlanes[id].Find("Zombie Selected").GetComponent<Image>().enabled = true;
        }
    }
}
