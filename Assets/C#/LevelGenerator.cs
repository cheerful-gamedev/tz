﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    [SerializeField] private GameObject tower;
    [SerializeField] private GameObject wall;
    [SerializeField] private int levelLenght = 10;
    [SerializeField] private Vector3 startPosition = new Vector3(-80,12,10);
    [SerializeField] private Vector3 towerOffset, wallOffset; // Поправки позиций, если пивоты не соответствуют центру объекта
    private int curLevelLenght;

    void Start()
    {
        SpawnIteration2();
        GetComponent<GameManager>().enabled = true;
    }

    void SpawnIteration2()
    {
        int towerOnLine = Random.Range(1,4);
        float verticalIndex = 0;
        Vector3 curPos;
        Vector3 spread;
        GameObject t_obj;
        GameObject w_obj;

        switch(towerOnLine)
        {
            case 1:
                curPos = new Vector3(startPosition.x+curLevelLenght*12, startPosition.y, startPosition.z);
                
                // Задаем разброс
                spread = new Vector3(Random.Range(-2f,2f), Random.Range(-2f,2f), 10);
                curPos += spread;
                t_obj = Instantiate(tower, curPos+towerOffset, Quaternion.identity);
                if(Random.Range(0,5) < 3)
                {
                    w_obj = Instantiate(wall, curPos+wallOffset, Quaternion.identity);
                }
            break;

            case 2:
                for(int i=-1; i<2; i++)
                {
                    if(i==0) continue;

                    verticalIndex = i; // 12-6=6  12-x=-6 -> 12-18=-6
                    curPos = new Vector3(startPosition.x+curLevelLenght*12, startPosition.y-verticalIndex*6, startPosition.z);
                    
                    // Задаем разброс
                    spread = new Vector3(Random.Range(-2f,2f), Random.Range(-2f,2f), 10);
                    curPos += spread;
                    t_obj = Instantiate(tower, curPos+towerOffset, Quaternion.identity);
                    if(Random.Range(0,5) < 3)
                    {
                        w_obj = Instantiate(wall, curPos+wallOffset, Quaternion.identity);
                    }
                }
            break;

            case 3:
                for(int i=-1; i<2; i++)
                {
                    verticalIndex = i;
                    curPos = new Vector3(startPosition.x+curLevelLenght*12, startPosition.y+verticalIndex*12, startPosition.z);
                    
                    // Задаем разброс
                    spread = new Vector3(Random.Range(-2f,2f), Random.Range(-2f,2f), 10);
                    curPos += spread;
                    t_obj = Instantiate(tower, curPos+towerOffset, Quaternion.identity);
                    if(Random.Range(0,5) < 3)
                    {
                        w_obj = Instantiate(wall, curPos+wallOffset, Quaternion.identity);
                    }
                }
            break;
            

        }

        curLevelLenght++;
        if(curLevelLenght > levelLenght)
            return;

        SpawnIteration2();
    }

    void SpawnIteration(int verticalIndex = 0)
    {
        Vector3 curPos = new Vector3(startPosition.x+curLevelLenght*12, startPosition.y-verticalIndex*12, startPosition.z);

        if(Random.Range(0,5) < 4)
        {
            Vector3 spread = new Vector3(Random.Range(-2f,2f), Random.Range(-2f,2f), 10);
            curPos += spread;
            GameObject t_obj = Instantiate(tower, curPos+towerOffset, Quaternion.identity);
            if(Random.Range(0,5) < 3)
            {
                GameObject w_obj = Instantiate(wall, curPos+wallOffset, Quaternion.identity);
            }
        }

        verticalIndex++;
        if(verticalIndex > 2)
        {
            verticalIndex = 0;
            curLevelLenght++;
            if(curLevelLenght > levelLenght)
                return;
        }

        SpawnIteration(verticalIndex);
    }
}
