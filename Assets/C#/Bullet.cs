﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [HideInInspector] public float damage;

    public void Initialization(float value) { damage = value; }

    void OnTriggerEnter(Collider col)
    {
        if(col.tag.Equals("Zomb"))
        {
            col.SendMessage("TakeDamage", damage);
            Destroy(gameObject);
        }
    }
}
