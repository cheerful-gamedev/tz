﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LayerController : MonoBehaviour
{
    [SerializeField] private int sortingOrderBase = 100;
    [SerializeField] private int offset = 0, zPos = 10;
    [SerializeField] private bool itIsStatic;
    [SerializeField] private Renderer myRenderer;
    [SerializeField] private ObjectType type;

    private enum ObjectType{ Tower, Wall, Bullet }

    private float timer, timerMax = .1f;

    void Start()
    {
        if(type == ObjectType.Tower || type == ObjectType.Wall)
            transform.position = new Vector3(transform.position.x, transform.position.y, zPos);
    }

    void LateUpdate()
    {
        timer -= Time.fixedDeltaTime;
        if(timer <= 0)
        {
            myRenderer.sortingOrder = (int)(sortingOrderBase - transform.position.y - offset);
            if(itIsStatic)
                Destroy(this);
        }
    }
}
