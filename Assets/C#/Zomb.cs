﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

using FTRuntime;
using FTRuntime.Yields;

public class Zomb : MonoBehaviour
{
    [SerializeField] private float rayDistance = 1.5f;
    [SerializeField] private SwfClipController clipController;
    [SerializeField] private SwfClip clip;
    [SerializeField] private Transform rayPoint;
    [SerializeField] private bool angry;
    [SerializeField] private AudioClip[] attackClips; // Все возможные звуки атаки для этого Зомбо
    [SerializeField] private AudioClip[] dieClips; // Все возможные звуки смерти для этого Зомбо

    [HideInInspector] public Obj myClass; // Информация о Зомбе
    [HideInInspector] public Vector3 nearestTower; // Ближайщая башня, заданная GameManager'ом
    [HideInInspector] public List<TowerPosition> TPs; // Класс с позициями вокруг башни

    private LayerMask towerLayer, wallLayer;

    NavMeshAgent agent;
    GameManager GM;
    RaycastHit hit;
    AudioSource audio;
    AudioClip selectedAttackClip; // Выбранный звук удара
    AudioClip selectedDieClip; // Выбранный звук удара
    Vector3 moveVector; // Вектор движения в сторону башни
    Vector3 startScale; // Начальные размеры персонажа
    private bool isDead; // Зомбо умер, играет анимация смерти

    void Awake()
    {
        towerLayer = LayerMask.GetMask("Tower");
        wallLayer = LayerMask.GetMask("Wall");
        GM = GameObject.Find("Game Manager").GetComponent<GameManager>();
        agent = GetComponent<NavMeshAgent>();
        audio = GetComponent<AudioSource>();
        StartCoroutine(clipChanger());
        startScale = transform.localScale;
    }

    // Маленький чит, чтобы звук спавна срабатывал полностью
    // и только потом происходила замена на звук удара
    IEnumerator clipChanger()
    {
        yield return new WaitForSeconds(.5f);
        audio.clip = selectedAttackClip;
    }

    void FixedUpdate()
    {
        if(isDead) return;

        moveVector = nearestTower - transform.position;
        moveVector.Normalize();

        Debug.DrawRay(rayPoint.position, moveVector, Color.red);
        // Деремся, если перед нами враг
        if (!clip.sequence.Equals("damage") || (clip.sequence.Equals("damage") && clip.currentFrame == clip.frameCount-1))
        {
            if (Physics.Raycast(rayPoint.position, moveVector, out hit, rayDistance, (towerLayer | wallLayer)) && hit.transform != transform)
            {
                agent.isStopped = true;
                agent.velocity = Vector3.zero;
                clipController.loopMode = SwfClipController.LoopModes.Once;
                clipController.Play("damage");
                hit.transform.SendMessage("TakeDamage", myClass.GetDamage());
                audio.Play();
            } // Бежим к цели, если перед нами никого нет
            else
            {
                if (!clip.sequence.Equals("run")) // Делаем проверку, чтобы анимация бега постоянно не начиналась с начала
                {
                    clipController.loopMode = SwfClipController.LoopModes.Loop;
                    clipController.Play("run");
                    agent.SetDestination(TPs[Random.Range(0, TPs.Count)].Position);
                    agent.isStopped = false;
                }
                
                // Границы перемещения Зомба
                if (transform.position.x < nearestTower.x)
                    transform.localScale = new Vector3(startScale.x, startScale.y, startScale.z);
                else if (transform.position.x > nearestTower.x)
                    transform.localScale = new Vector3(-1*startScale.x, startScale.y, startScale.z);
            }
        }
    }

    public void Initialization(Obj obj)
    {
        myClass = obj;
        agent.speed = myClass.GetSpeed();
        // Случайным образом выбираем звуки для Зомбо
        selectedAttackClip = attackClips[Random.Range(0,attackClips.Length)];
        selectedDieClip = dieClips[Random.Range(0,dieClips.Length)];

        /*clipController.rateScale = myClass.GetSpeed()/5;*/ 
    }

    public void TakeDamage(float damage)
    {
        if(isDead) return;
        myClass.TakeDamage(damage);
        if(myClass.GetCurHealth() > 0)
            clipController.Play("stop");
    }

    public void TakeDamage() // СТАРОЕ
    {
        transform.localScale = new Vector3(-transform.localScale.x, startScale.y, startScale.z);
        clipController.Play("stop");
    }

    public void Die()
    {
        if(isDead) return;
        isDead = true;
        agent.isStopped = true;
        agent.velocity = Vector3.zero;

        GM.DeleteZomb(myClass);
        Destroy(GetComponent<Collider>());
        // GetComponent<Collider>().enabled = false;
        agent.enabled = false;
        clipController.loopMode = SwfClipController.LoopModes.Once;
        clipController.transform.parent = transform.parent;
        clipController.Play("death_0" + Random.Range(1, 3));
        AudioSource clipAudio = clipController.GetComponent<AudioSource>();
        clipAudio.clip = selectedDieClip;
        clipAudio.Play();
        Destroy(clipController.gameObject, 10);
        Destroy(gameObject);
    }
    public void SetNearestTower(Vector3 pos) { nearestTower = pos; }
    public void SetNearestTower(List<TowerPosition> tps)
    {
        TPs = tps;
        agent.SetDestination(TPs[Random.Range(0, TPs.Count)].Position);
        agent.isStopped = false;
    }
}
