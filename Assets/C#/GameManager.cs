﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject[] zombPrefabs;
    //[HideInInspector]
    public List<Obj> towers = new List<Obj>();
    //[HideInInspector]
    public List<Obj> walls = new List<Obj>();
    //[HideInInspector]
    public List<Obj> zombs = new List<Obj>();
    // [HideInInspector] 
    public List<TowerPosition> TPs; // Класс с позициями вокруг башни
    [SerializeField] private Transform zomboTower; // Завод из которого спавнятся Зомбо
    [SerializeField] private NavMeshSurface nav; // Компонент, создающий navmesh
    [SerializeField] private Text[] cash; // Деньги и алмазы, которыми обладает игрок
    [SerializeField] private GameObject coin; // Гребаная монетка, которая будет появляться в бою
    [SerializeField] private Slider waySlider; // Бар, отображающий пройденный путь до конца уровня
    [SerializeField] private AudioClip winSong, loseSong; // Звуки победы и проигрыша

    public MenuInfo playerData; // Класс-файл, хранящий данные
    [SerializeField] private List<GameObject> windowsGO = new List<GameObject>(); // Объекты относящиеся к окнам в конце боя
    private float gameOverTimer; // Время без Зомбов на сцене
    private bool gameFinished; // Бой окончена
    Vector3 nearestTower;
    Vector3 nearestZomb;
    Vector3 spawnPos = new Vector3(-107, 1, 5);
    Vector3 towersSidePos = new Vector3(100, 1, 5);
    Vector3 lastTowerPosition; // Позиция последней башни для расчета пройденного пути (waySlider)
    AudioSource audio;

    // Статистика боя, для итогового окна
    private float timeLeft;
    private int zombsSpawned, towersDestroyed;
    [SerializeField] private Text timer; // Показывает секунды до окончания боя

    CameraFollow CF;

    void OnEnable()
    {
        playerData.GetStartKit(); // Обновляем данные в класс-файле

        spawnPos = zomboTower.position;
        CF = GameObject.Find("Main Camera").GetComponent<CameraFollow>();
        nearestZomb = spawnPos;

        // о - временный кэш, для хранения найденных на сцене объектов
        List<GameObject> o = new List<GameObject>(GameObject.FindGameObjectsWithTag("Tower")); // Находим башни
        foreach (GameObject gm in o)
            towers.Add(new Obj(gm, 3, 7, 3, 0, .1f, true));
        o.Clear();

        o.AddRange(GameObject.FindGameObjectsWithTag("Wall")); // Находим стены
        foreach(GameObject gm in o)
            walls.Add(new Obj(gm, 1, 2, 1, 0, 0));
        o.Clear();

        nav.BuildNavMesh();
        CalculateNearestTower();

        timeLeft = 0;
        zombsSpawned = 0;
        towersDestroyed = 0;

        cash[0].text = playerData.GetMoney().ToString();
        cash[1].text = playerData.GetDiamonds().ToString();

        // Нахождение дальней башни
        float maxDist = -1;
        for(int i=0; i<towers.Count; i++)
        {
            Vector3 curTowerPosition = towers[i].GetGameObject().transform.position;
            float d = Vector3.Distance(spawnPos, curTowerPosition);
            if(d > maxDist)
            {
                maxDist = d;
                lastTowerPosition = curTowerPosition;
            }
        }

        audio = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        timeLeft += Time.fixedDeltaTime;
        // Ищем ближайшего зомба
        float dist = -1;
        for(int i=0; i<zombs.Count; i++)
        {
            float d = Vector3.Distance(towersSidePos, zombs[i].GetGameObject().transform.position);
            if (dist == -1 || d < dist)
            {
                dist = d;
                nearestZomb = zombs[i].GetGameObject().transform.position;
            }
        }

        // Отправляем координату камере
        CF.nearestZomb = nearestZomb;
        
        // Обновление бара пройденной позиции
        float sliderValue = Vector3.Distance(spawnPos, nearestZomb)/(Vector3.Distance(spawnPos, lastTowerPosition));
        waySlider.value = sliderValue > waySlider.value ? sliderValue : waySlider.value;

        // Отправляем координаты ближайшего Зомбо
        foreach(Obj obj in towers)
        {
            obj.GetGameObject().SendMessage("SetNearestZomb", nearestZomb);
            obj.PlusRegeneration();
        }

        // Просто делаем окно таймера пустым
        timer.text = "";

        if(gameFinished) return;
        // Проверяем условия для окончания боя
        if(towers.Count == 0) // Все башни повержены
        {
            StartCoroutine(EndGame(true));
            return;
        }
        else if(zombs.Count == 0 && zombsSpawned > 0) // Зомбо не спавнили более 3 секунд
        {
            gameOverTimer += Time.fixedDeltaTime;
            timer.text = Mathf.RoundToInt(3 - gameOverTimer).ToString();
            if(gameOverTimer >= 3)
                StartCoroutine(EndGame(false));
        }
        else
            gameOverTimer = 0;
    }

    public void CalculateNearestTower(Obj destroyedTower = null)
    {
        if(destroyedTower != null)
        {
            RefreshCash(Random.Range(100,1000), 0, destroyedTower.GetGameObject().transform.position);

            towers.Remove(destroyedTower); // Удаляем разрушенную Башню
            towersDestroyed++;
        }

        // Ищем ближайшую башню
        float minDist = -1;
        int index = -1;
        for(int i=0; i<towers.Count; i++)
        {
            float d = Vector3.Distance(spawnPos, towers[i].GetGameObject().transform.position);
            if (minDist == -1 || d < minDist)
            {
                minDist = d;
                nearestTower = towers[i].GetRandomPosition();//towers[i].GetGameObject().transform.position;
                TPs = towers[i].GetTowerPositions();
                index = i;
            }
        }

        // Отправляем координаты ближайшей башни
        foreach (Obj obj in zombs)
        {
            obj.GetGameObject().SendMessage("SetNearestTower", TPs);
            obj.GetGameObject().SendMessage("SetNearestTower", nearestTower);
        }
    }

    // Изменения кол-во валюты в бою
    public void RefreshCash(int money, int diamonds, Vector3 pos)
    {
        playerData.SetMoney(playerData.GetMoney() + money * playerData.GetMoney_k());
        playerData.SetDiamonds(playerData.GetDiamonds() + diamonds * playerData.GetDiamonds_k());

        // if(pos == new Vector3(-1000, -1000, -1000)) // То это из окна
        // Instantiate(coin, Camera.main.WorldToScreenPoint(pos), Quaternion.identity);

        cash[0].text = playerData.GetMoney().ToString();
        cash[1].text = playerData.GetDiamonds().ToString();
        DataBackuper.WriteToDataFile();
    }

    public void DeleteWall(Obj obj) { walls.Remove(obj); }
    public void DeleteZomb(Obj obj) { zombs.Remove(obj); }

    public void AddNewZomb(int zombIndex = 0)
    {
        ZomboInfo zomb = playerData.GetZombo(zombIndex);
        Obj obj = new Obj(null, zomb.GetDamage(), zomb.GetHealth(), zomb.GetSpeed(), zomb.GetArmor(), 0);
        zombsSpawned++;
        float deep = Random.Range(-3f, 3f);
        obj.SetGameObject(Instantiate(zombPrefabs[zombIndex], new Vector3(spawnPos.x + deep, spawnPos.y + deep, 0), Quaternion.identity));
        obj.GetGameObject().SendMessage("SetNearestTower", TPs);
        obj.GetGameObject().SendMessage("SetNearestTower", nearestTower);
        zombs.Add(obj);

        RefreshCash(1, 0, obj.GetGameObject().transform.position);
    }

    IEnumerator EndGame(bool isWin)
    {
        gameFinished = true;
        GetComponent<Taper>().enabled = false;
        int min = (int)timeLeft / 60;
        int sec = (int)timeLeft % 60;
        int money = Mathf.RoundToInt(towersDestroyed * 10 * (zombsSpawned/10) / timeLeft) * playerData.GetMoney_k();
        int diamonds = Mathf.RoundToInt(towersDestroyed * 10  / timeLeft) * playerData.GetDiamonds_k();

        foreach(Obj zomb in new List<Obj>(zombs))
        {
            zomb.TakeDamage(100000);
            yield return new WaitForSeconds(.005f);
        }
            
        yield return new WaitForSeconds(.5f);

        if(isWin)
        {
            windowsGO[0].SetActive(true);
            Debug.Log("Win");
            money *= 10;
            audio.clip = winSong;
            audio.Play();
        }
        else
        {
            windowsGO[1].SetActive(true);
            Debug.Log("Game Over");
            audio.clip = loseSong;
            audio.Play();
        }

        money += zombsSpawned;
        RefreshCash(zombsSpawned, 0, new Vector3(-1000, -1000, -1000));

        // ОБновляем данные в класс-файле
        playerData.SetMoney(playerData.GetMoney() + money);
        playerData.SetDiamonds(playerData.GetDiamonds() + diamonds);
        DataBackuper.WriteToDataFile();

        Debug.Log(timeLeft);
        windowsGO[2].transform.Find("Time_text").GetComponent<Text>().text = min + ":" + sec;
        windowsGO[2].transform.Find("Dies_text").GetComponent<Text>().text = zombsSpawned.ToString();
        windowsGO[2].transform.Find("Towers_text").GetComponent<Text>().text = towersDestroyed.ToString();
        windowsGO[2].transform.Find("Money_text").GetComponent<Text>().text = money.ToString();
        windowsGO[2].transform.Find("Diamond_text").GetComponent<Text>().text = diamonds.ToString();
        windowsGO[2].transform.Find("MoneyK_text").GetComponent<Text>().text = "X"+playerData.GetMoney_k();
        windowsGO[2].transform.Find("DiamondK_text").GetComponent<Text>().text = "X"+playerData.GetDiamonds_k();
        yield return new WaitForSeconds(17/24);
        windowsGO[3].SetActive(false);
        windowsGO[2].SetActive(true);
    }

    public void LoadMenuScene() { SceneManager.LoadScene(0); }
}
