﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FTRuntime;
using FTRuntime.Yields;

public class Tower : MonoBehaviour
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private Transform gunPoint; // Точка спавна снаряда
    [SerializeField] private float power; // Скорость полета снаряда
    [SerializeField] private SwfClipController clipController;
    [SerializeField] private SwfClip clip;
    [SerializeField] private AudioClip[] attackClips; // Все возможные звуки атаки для этого Зомбо
    [SerializeField] private AudioClip[] dieClips; // Все возможные звуки смерти для этого Зомбо

    private Obj myClass; // Информация о Башне
    private int destructionIndex = 1; // Индекс степени разрушенности
    private bool destroyed; // Башня уже уничтожена
    private float takeDamageTimer; // Время с последнего удара
    Vector3 nearestZomb; // Зомбо, в которого нужно стрелять

    GameManager GM;
    AudioSource audio;
    AudioClip selectedAttackClip; // Выбранный звук удара
    AudioClip selectedDieClip; // Выбранный звук удара

    void Start()
    {
        StartCoroutine(Shooting());
    }
    void Awake() { GM = GameObject.Find("Game Manager").GetComponent<GameManager>(); audio = GetComponent<AudioSource>(); }

    public void Initialization(Obj obj)
    {
        this.enabled = true;
        myClass = obj;
        // Случайным образом выбираем звуки для Зомбо
        selectedAttackClip = attackClips[Random.Range(0,attackClips.Length)];
        selectedDieClip = dieClips[Random.Range(0,dieClips.Length)];
        audio.clip = selectedAttackClip;
    }

    public void SetNearestZomb(Vector3 pos) { nearestZomb = pos; }

    void Update() { takeDamageTimer += Time.deltaTime; }

    public void TakeDamage(float damage)
    { 
        if(destroyed) return;

        takeDamageTimer = 0;
        if(clipController.isStopped)
            clipController.Play("damage_0"+destructionIndex+"_0"+Random.Range(1,3));
        myClass.TakeDamage(damage);
    }

    public void Regeneration()
    {
        if(takeDamageTimer < .3f) return;

        if(destructionIndex == 2)
        {
            destructionIndex = 1;
            clipController.playMode = SwfClipController.PlayModes.Backward;
            clipController.Play("destruction_"+destructionIndex);
            myClass.SetCurHealth(.01f);
        }
        else if(destructionIndex == 3 && !destroyed)
        {
            destructionIndex = 2;
            clipController.playMode = SwfClipController.PlayModes.Backward;
            clipController.Play("destruction_"+destructionIndex);
            myClass.SetCurHealth(.01f);
        }
    }

    public void Die()
    { 
        if(destructionIndex == 1)
        {
            clipController.playMode = SwfClipController.PlayModes.Forward;
            clipController.Play("destruction_"+destructionIndex);
            destructionIndex = 2;
            myClass.SetCurHealth(myClass.GetHealth());
        }
        else if(destructionIndex == 2)
        {
            clipController.playMode = SwfClipController.PlayModes.Forward;
            clipController.Play("destruction_"+destructionIndex);
            destructionIndex = 3;
            myClass.SetCurHealth(myClass.GetHealth());
        }
        else if(destructionIndex == 3)
        {
            destroyed = true;
            clipController.transform.parent = transform.parent;
            clipController.playMode = SwfClipController.PlayModes.Forward;
            clipController.Play("destruction_"+destructionIndex);
            GM.CalculateNearestTower(myClass);
            Destroy(gameObject);
        }
    }

    IEnumerator Shooting()
    {
        // audio.Play();
        GameObject obj = Instantiate(bullet, gunPoint.position, Quaternion.identity);
        Destroy(obj, 3);
        Vector3 shotPower = nearestZomb - gunPoint.position;
        shotPower.y += Random.Range(-2f, 2f);
        shotPower.Normalize();
        obj.GetComponent<Rigidbody>().velocity = shotPower * power;
        obj.SendMessage("Initialization", myClass.GetDamage());
        yield return new WaitForSeconds(1/myClass.GetSpeed());
        StartCoroutine(Shooting());
    }
}
