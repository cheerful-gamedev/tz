﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Menu Info", menuName = "Menu Info")]
[System.Serializable]
public class MenuInfo : ScriptableObject
{
    [SerializeField] private MenuInfo Shop; // Класс-файл магазина, хранящего в себе все "предметы" для покупки
    [SerializeField] private int Money, Diamonds, Videos;
    [SerializeField] private int Money_k = 1, Diamonds_k = 1;
    [SerializeField] private int PlanetLevel, // Уровень планеты, который куплен (кол-во купленных планет слева направо)
    ZomboLevel; // Уровень Зомбо, который куплен (кол-во купленных планет слева направо)
    [SerializeField] private int CurSelectedZomboIndex; // Выделенный на данный момент Зомбо
    [SerializeField] private List<Parameter> CashUpgrades = new List<Parameter>(); // Апгрейды коэф. получаемой валюты
    [SerializeField] private List<ZomboInfo> AllZombos = new List<ZomboInfo>();

    // Получаем все бысплатные "предметы" из магазина
    public void GetStartKit()
    {
        // Обновляем стартовые значения и значения параметров, вдруг они обновились
        foreach(ZomboInfo zombo in this.AllZombos)
        {
            ZomboInfo shopZm = Shop.AllZombos.Find(z => z.GetID() == zombo.GetID());
            if(shopZm != null)
            {
                zombo.SetAllParameters(shopZm);
                zombo.SetStartedParameters();
                
                // Ищем купленные нами параметры, и проверяем их на соответствие
                foreach(Parameter param in new List<Parameter>(zombo.GetParameters())) // Нужен new, так как мы изменяем List в foreach
                {
                    Parameter shopParam = shopZm.GetParameters().Find(p => p.GetID() == param.GetID() && p.GetName().Equals(param.GetName()));
                    if (shopParam != null)
                    {
                        zombo.GetParameters().Remove(param);
                        zombo.GetParameters().Add(new Parameter(shopParam));
                    }
                    //else // Если из класс-файла магазина данный параметр был удален
                        //zombo.GetParameters().Remove(param);

                }

                // Ищем бесплатные параметры в класс-файле магазина и добавляем к Зомбо
                foreach (Parameter param in shopZm.GetParameters().FindAll(p => p.GetPrice() == 0))
                    if(zombo.GetParameters().Find(p => p.GetName().Equals(param.GetName())) == null) // Если подобный параметр еще не куплен
                        zombo.GetParameters().Add(new Parameter(param));
            }
            //else // Можно добавить сюда удаление Зомбо, если он не найден в класс-файле магазина

            zombo.RefreshAllCharacteristics();
        }

        // Поиск бесплатных Зомбо и параметров для них
        foreach(ZomboInfo shopZm in  Shop.AllZombos)
        {
            if(shopZm.GetPrice() == 0 && this.AllZombos.Find(z => z.GetID() == shopZm.GetID()) == null) // Если Зомбо бесплатный
            {
                ZomboInfo newZomb = new ZomboInfo(shopZm); // Создаем новый класс, и присваиваем начальные значения
                this.AllZombos.Add(newZomb); // Добавляем нового Зомбо в класс-файл игрока

                // Добавляем бесплатные параметры
                foreach (Parameter param in shopZm.GetParameters().FindAll(p => p.GetPrice() == 0))
                    newZomb.GetParameters().Add(new Parameter(param));

                newZomb.RefreshAllCharacteristics();
            }
        }

        // Обновляем имеющиеся апгрейды валюты, вдруг они обновились
        foreach(Parameter upgrade in new List<Parameter>(CashUpgrades)) // Нужен new, так как мы изменяем List в foreach
        {
            Parameter shopUp = Shop.GetCashUpgradeByName(upgrade.GetName(), upgrade.GetID());
            if (shopUp != null)
                this.BuyCashUpgrade(shopUp);
            //else // Если из класс-файла магазина данный апгрейд был удален
                //CashUpgrades.Remove(upgrade);

        }

        // Ищем бесплатные апгрейды валют в класс-файле магазина и добавляем к Зомбо
        foreach (Parameter shopUp in Shop.CashUpgrades.FindAll(p => p.GetPrice() == 0))
            if(this.GetCashUpgradeByName(shopUp.GetName()) == null) // Если подобный апгрейд еще не куплен
                this.BuyCashUpgrade(shopUp);
    }

    // Добавление нового Зомбо в список
    public void BuyNewZombo(int id)
    {
        ZomboInfo shopZm = Shop.AllZombos.Find(z => z.GetID() == id);
        ZomboInfo newZomb = new ZomboInfo(shopZm); // Создаем новый класс, и присваиваем начальные значения
        newZomb.Sprite = shopZm.Sprite;
        newZomb.SetID(shopZm.GetID());
        newZomb.SetStartedParameters(); // Присваиваем начальные значения конечным
        newZomb.SetPriceType(shopZm.GetPriceType());
        newZomb.SetTapMultiplier(shopZm.GetTapMultiplier());
        this.AllZombos.Add(newZomb); // Добавляем нового Зомбо в класс-файл игрока

        // Добавляем бесплатные параметры
        foreach (Parameter param in shopZm.GetParameters())
        {
            if (param.GetPrice() == 0)
            {
                newZomb.GetParameters().Add(param);
                newZomb.SetParameterValueByName(param.GetName(), param.GetValue());
            }
        }
    }

    // Добавление парметра в список
    public void BuyCashUpgrade(Parameter param)
    {
        // Удаляем уже существующий параметр по имени
        this.CashUpgrades.Remove(this.CashUpgrades.Find(p => p.GetName().Equals(param.GetName())));
        this.CashUpgrades.Add(new Parameter(param));
        switch(param.GetName())
        {
            case "Money": this.SetMoney_k((int)param.GetValue()); break;
            case "Diamonds": this.SetDiamonds_k((int)param.GetValue()); break;
        }
    }

    // Получаем Апгрейд валюты по имени
    public Parameter GetCashUpgradeByName(string name) { return CashUpgrades.Find(p => p.GetName().Equals(name)); }

    // Получаем Апгрейд валюты по имени и индексу
    public Parameter GetCashUpgradeByName(string name, int id) { return CashUpgrades.Find(p => p.GetName().Equals(name) && p.GetID() == id); }

    // Геттеры первого уровня
    public int GetMoney() { return this.Money; }
    public int GetDiamonds() { return this.Diamonds; }
    public int GetVideos() { return this.Videos; }
    public int GetMoney_k() { return this.Money_k; }
    public int GetDiamonds_k() { return this.Diamonds_k; }
    public int GetPlanetLevel() { return this.PlanetLevel; }
    public int GetZomboLevel() { return this.Money; }
    public int GetCurSelectedZombo() { return this.CurSelectedZomboIndex; }
    /// <summary>
    /// Для поиска Зомбо в класс-файле игрока
    /// </summary>
    public ZomboInfo GetZombo(int id) { return this.AllZombos.Find(z => z.GetID() == id); }
    /// <summary>
    /// Для поиска Зомбо в класс-файле магазина (т.к. один и тот же Зомбо может быть за разные валюты)
    /// </summary>
    public ZomboInfo GetZombo(int id, PriceType priceType) { return this.AllZombos.Find(z => z.GetID() == id && z.GetPriceType() == priceType); }
    public List<ZomboInfo> GetAllZombo() { return this.AllZombos; }

    // Сеттеры первого уровня
    public void SetMoney(int value) { this.Money = value; }
    public void SetDiamonds(int value) { this.Diamonds = value; }
    public void SetVideos(int value) { this.Videos = value; }
    public void SetMoney_k(int value) { this.Money_k = value; }
    public void SetDiamonds_k(int value) { this.Diamonds_k = value; }
    public void SetPlanetLevel(int value) { this.PlanetLevel = value; }
    public void SetZomboLevel(int value) { this.Money = value; }
    public void SetCurSelectedZombo(int value) { this.CurSelectedZomboIndex = value; }
}
