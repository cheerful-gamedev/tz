﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private float minZoom = 5, maxZoom = 30;
    [HideInInspector] public Vector3 nearestZomb; // Ближайщий к последней башне Зомби
    private Camera camera;
    private Vector2 lastTouchPosition; // Начальная позиция свайпа для приближения или отдоления камеры
    private float touchTimer;

    void Awake() { camera = GetComponent<Camera>(); }

    void FixedUpdate()
    {
        nearestZomb.y = 0;
        nearestZomb.z = -10;
        transform.position = Vector3.Lerp(transform.position, nearestZomb, Time.deltaTime);
    }

    void Update()
    {
        if(Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if(touch.phase == TouchPhase.Began)
            {
                lastTouchPosition = touch.position;
                touchTimer = 0;
            }
            else if(touch.phase == TouchPhase.Stationary)
                touchTimer += Time.deltaTime;
            else if(touchTimer > .2f && touch.phase == TouchPhase.Moved)
            {
                camera.orthographicSize += (lastTouchPosition.y - touch.position.y)/3;
                camera.orthographicSize = Mathf.Max(camera.orthographicSize, minZoom);
                camera.orthographicSize = Mathf.Min(camera.orthographicSize, maxZoom);
                lastTouchPosition = touch.position;
            }
            else if(touch.phase == TouchPhase.Ended)
                touchTimer = 0;
        }
        else
            touchTimer = 0;

#if UNITY_EDITOR
        if(Input.GetMouseButtonDown(0))
            lastTouchPosition = Input.mousePosition;
        else if(Input.GetMouseButton(0))
        {
            camera.orthographicSize += lastTouchPosition.y - Input.mousePosition.y;
            camera.orthographicSize = Mathf.Max(camera.orthographicSize, minZoom);
            camera.orthographicSize = Mathf.Min(camera.orthographicSize, maxZoom);
            lastTouchPosition = Input.mousePosition;
        }
#endif
    }
}