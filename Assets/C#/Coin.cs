﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    private Vector2 vector;

    void Start()
    {
        vector = new Vector2(100, -40) - (Vector2)transform.position;
        vector.Normalize();
    }

    void FixedUpdate()
    {
        transform.Translate(vector);
    }
}
