﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[System.Serializable]
public class ZomboInfo
{
    public Sprite Sprite;
    [SerializeField] private PriceType PriceType;
    [SerializeField] private int ID = 0, Price = 0;
    [SerializeField] private float StartDamage = 1, Damage = 1;
    [SerializeField] private float StartHealth = 3, Health = 3;
    [SerializeField] private float StartSpeed = 10, Speed = 10;
    [SerializeField] private float StartArmor = 0, Armor = 0;
    [SerializeField] private int TapMultiplier = 1; // Множитель тапа
    [SerializeField] private List<Parameter> Parameters = new List<Parameter>();

    public ZomboInfo(ZomboInfo zm)
    {
        this.Sprite = zm.Sprite;
        this.Price = zm.Price;
        this.StartDamage = zm.StartDamage;
        this.Damage = zm.Damage;
        this.StartHealth = zm.StartHealth;
        this.Health = zm.Health;
        this.StartSpeed = zm.StartSpeed;
        this.Speed = zm.Speed;
        this.StartArmor = zm.StartArmor;
        this.Armor = zm.Armor;
        this.TapMultiplier = zm.TapMultiplier;
    }

    // Задаем все значения Зомбо
    public void SetAllParameters(ZomboInfo zm)
    {
        this.Sprite = zm.Sprite;
        this.Price = zm.Price;
        this.StartDamage = zm.StartDamage;
        this.Damage = zm.Damage;
        this.StartHealth = zm.StartHealth;
        this.Health = zm.Health;
        this.StartSpeed = zm.StartSpeed;
        this.Speed = zm.Speed;
        this.StartArmor = zm.StartArmor;
        this.Armor = zm.Armor;
        this.TapMultiplier = zm.TapMultiplier;
    }

    // Присвоение всех стартовых значений
    public void SetStartedParameters()
    {
        this.Damage = this.StartDamage;
        this.Health = this.StartHealth;
        this.Speed = this.StartSpeed;
        this.Armor = this.StartArmor;
        this.TapMultiplier = 1;
    }

    // Перерасчет значений параметров с учетом купленных апгрейдов
    public void RefreshAllCharacteristics()
    {
        this.SetStartedParameters();

        this.Damage = this.GetUpgradeValueByName("Damage") != null ? this.Damage + (float)this.GetUpgradeValueByName("Damage") : this.Damage;
        this.Health = this.GetUpgradeValueByName("Health") != null ? this.Health + (float)this.GetUpgradeValueByName("Health") : this.Health;
        this.Speed = this.GetUpgradeValueByName("Speed") != null ? this.Speed + (float)this.GetUpgradeValueByName("Speed") : this.Speed;
        this.Armor = this.GetUpgradeValueByName("Armor") != null ? this.Armor + (float)this.GetUpgradeValueByName("Armor") : this.Armor;
        this.TapMultiplier = this.GetUpgradeValueByName("TapMultiplier") != null ? this.TapMultiplier + (int)(float)this.GetUpgradeValueByName("TapMultiplier") : this.TapMultiplier;
    }

    // Геттеры второго уровня
    public PriceType GetPriceType() { return this.PriceType; }
    public int GetID() { return this.ID; }
    public int GetPrice() { return this.Price; }
    public float GetDamage() { return this.Damage; }
    public float GetHealth() { return this.Health; }
    public float GetSpeed() { return this.Speed; }
    public float GetArmor() { return this.Armor; }
    public float GetStartDamage() { return this.StartDamage; }
    public float GetStartHealth() { return this.StartHealth; }
    public float GetStartSpeed() { return this.StartSpeed; }
    public float GetStartArmor() { return this.StartArmor; }
    public int GetTapMultiplier() { return this.TapMultiplier; }

    // Сеттеры второго уровня
    public void SetPriceType(PriceType value) { this.PriceType = value; }
    public void SetID(int value) { this.ID = value; }
    public void SetPrice(int value) { this.Price = value; }
    public void SetDamage(float value) { this.Damage = value; }
    public void SetHealth(float value) { this.Health = value; }
    public void SetSpeed(float value) { this.Speed = value; }
    public void SetArmor(float value) { this.Armor = value; }
    public void SetStartDamage(float value) { this.StartDamage = value; }
    public void SetStartHealth(float value) { this.StartHealth = value; }
    public void SetStartSpeed(float value) { this.StartSpeed = value; }
    public void SetStartArmor(float value) { this.StartArmor = value; }
    public void SetTapMultiplier(int value) { this.TapMultiplier = value; }

    // Получаем значение параметра, по имени
    public object GetParameterValueByName(string name)
    {
        object value = null;
        switch(name)
        {
            case "PriceType": value = this.PriceType; break;
            case "ID": value = this.ID; break;
            case "Price": value = this.Price; break;
            case "Damage": value = this.Damage; break;
            case "Health": value = this.Health; break;
            case "Speed": value = this.Speed; break;
            case "Armor": value = this.Armor; break;
            case "StartDamage": value = this.StartDamage; break;
            case "StartHealth": value = this.StartHealth; break;
            case "StartSpeed": value = this.StartSpeed; break;
            case "StartArmor": value = this.StartArmor; break;
            case "TapMultiplier": value = this.TapMultiplier; break;
        }

        return value;
    }

    // Получаем значение апгрейда, по имени
    public object GetUpgradeValueByName(string name)
    {
        Parameter param = this.Parameters.Find(p => p.GetName().Equals(name));
        if (param != null)
            return param.GetValue();
        else
            return null;
    }

    // Получаем значение параметра, по имени
    public Parameter GetParameterByName(string name) { return this.Parameters.Find(p => p.GetName().Equals(name)); }
    // Получаем значение параметра, по имени и порядковому номеру
    public Parameter GetParameterByName(string name, int id) { return this.Parameters.Find(p => p.GetName().Equals(name) && p.GetID() == id); }

    // Обновляем значение параметра по имени
    public void SetParameterValueByName(string name, object value)
    {
        switch(name)
        {
            case "PriceType": this.PriceType = (PriceType)value; break;
            case "ID": this.ID = (int)value; break;
            case "Price": this.Price = (int)value; break;
            case "Damage": this.Damage = (float)value; break;
            case "Health": this.Health = (float)value; break;
            case "Speed": this.Speed = (float)value; break;
            case "Armor": this.Armor = (float)value; break;
            case "StartDamage": value = (float)value; break;
            case "StartHealth": value = (float)value; break;
            case "StartSpeed": value = (float)value; break;
            case "StartArmor": value = (float)value; break;
            case "TapMultiplier": this.TapMultiplier = (int)(float)value; break;
        }
    }

    // Добавление парметра в список
    public void BuyParameter(Parameter param)
    {
        // Удаляем уже существующий параметр по имени
        this.Parameters.Remove(this.Parameters.Find(p => p.GetName().Equals(param.GetName())));
        this.Parameters.Add(new Parameter(param));
        RefreshAllCharacteristics();
    }

    public List<Parameter> GetParameters() { return this.Parameters; }
}

[System.Serializable]
public class Parameter // Параметр Зомбо
{
    [SerializeField] private string Name = "Armor";
    [SerializeField] private int ID = 0; // Порядковый номер уровня прокачки
    [SerializeField] private PriceType PriceType;
    [SerializeField] private int Price = 100;
    [SerializeField] private float Value = 1; // Присваимое значение параметра после покупки

    public Parameter() { }

    public Parameter(Parameter param)
    {
        this.Name = param.Name;
        this.ID = param.ID;
        this.PriceType = param.PriceType;
        this.Price = param.Price;
        this.Value = param.Value;
    }

    // Геттеры третьего уровня
    public string GetName() { return this.Name; }
    public PriceType GetPriceType() { return this.PriceType; }
    public int GetID() { return this.ID; }
    public int GetPrice() { return this.Price; }
    public float GetValue() { return this.Value; }

    // Сеттеры третьего уровня
    public void SetName(string value) { this.Name = value; }
    public void SetPriceType(PriceType value) { this.PriceType = value; }
    public void SetID(int value) { this.ID = value; }
    public void SetPrice(int value) { this.Price = value; }
    public void SetValue(float value) { this.Value = value; }
}

public enum PriceType
{
    Money,
    Diamond,
    Video
}
